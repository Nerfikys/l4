package L4_04.repositry;

import L4_04.domain.Account;
import java.io.IOException;
public interface Repository {
   void write(Account out) throws IOException;
   void  read() throws IOException;

}
