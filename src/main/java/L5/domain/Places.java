package L5.domain;

public class Places {
    private  int id;// id места
    private int row;//номер ряда
    private int number;//номер места
    private int Kat; //простое\дороже\вип
    public Places(){}
    public Places(int id,int row, int number,int kat){
        this.id=id;
        this.row=row;
        this.Kat=Kat;
        this.number=number;
    }

    public int getId() {
        return id;
    }

    public int getKat() {
        return Kat;
    }

    public int getNumber() {
        return number;
    }

    public int getRow() {
        return row;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setKat(int kat) {
        Kat = kat;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
