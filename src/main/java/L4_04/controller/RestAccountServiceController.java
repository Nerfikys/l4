package L4_04.controller;

import L4_04.exception.NotEnoughMoneyException;
import L4_04.exception.UnknownAccountException;
import L4_04.repositry.Repository;
import L4_04.service.AccountService;
import L4_04.service.store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
public class RestAccountServiceController {
    @Autowired
    private AccountService Service;

    @GetMapping("/balance")
    public ResponseEntity<Double> balance(@RequestParam("id") int id) throws UnknownAccountException {
        return new ResponseEntity<Double>(Service.balance(id), HttpStatus.OK);
    }

    @GetMapping("/withdraw/{id}/{amount}")
    public ResponseEntity<String> withdraw(@PathVariable("id") int id,@PathVariable("amount") double amount) throws IOException, NotEnoughMoneyException, UnknownAccountException {
        Service.withdraw(id, amount);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping("/deposit")
    public ResponseEntity<String> deposit(@RequestParam("id") int id,@RequestParam("amount") double amount) throws UnknownAccountException, IOException {
        Service.deposit(id,amount);
            return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/transfer")
    public ResponseEntity<String> transfer(@RequestBody Values values) throws NotEnoughMoneyException, UnknownAccountException, IOException {
        Service.transfer(values.getFromId(),values.getToId(),values.getAmount());
        return new ResponseEntity<>(HttpStatus.OK);
    }
    static class Values{
        private int fromId;
        private int toId;
        private double amount;

        public Values() {

        }
        public Values(int fromId,int toId,double amountOd){
            this.fromId=fromId;
            this.toId=toId;
            this.amount=amount;
        }
        public int getFromId(){
            return fromId;
        }

        public int getToId() {
            return toId;
        }


        public double getAmount() {
            return amount;
        }
    }
}

