package L4_01.service;


import L4_01.exception.NotEnoughMoneyException;
import L4_01.exception.UnknownAccountException;

public interface AccountService {
   static void withdraw (int account, double amount) throws UnknownAccountException, NotEnoughMoneyException{};
   static double balance(int account) throws UnknownAccountException{return 0;};
   static void deposit(int account, double amount) throws UnknownAccountException, NotEnoughMoneyException{};
   static void transfer(int from, int to, double amount) throws UnknownAccountException, NotEnoughMoneyException{};
}
