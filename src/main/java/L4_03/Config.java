package L4_03;

import L4_03.repositry.FileRepository;
import L4_03.repositry.Repository;
import L4_03.service.AccountService;
import L4_03.service.FileAccountService;
import L4_03.service.store;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;


@Configuration
public class Config {


    @Bean
    public Repository getRepository(){ return new FileRepository(); }

    @Bean
    public AccountService getAccountService(){
        return new FileAccountService();
    }

    @Bean
    public store getStore(){ return new store(); }

    @Bean
    public Menu getMenu(){return new Menu(); }
}
