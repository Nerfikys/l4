package L5.service;


import L5.domain.Pologenie;
import L5.exception.UnknownInputException;
import L5.repositry.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.Iterator;
import java.util.Random;

@Component
public class FileTicketService implements TicketService {

    @Autowired
    private Repository Repository;

    @Override
    public void buyTicket(String day, String nameFilm, String place, String timeStartFilm,int Kategory) throws UnknownInputException, IOException {
        Pologenie pologenie = Poisk(day,nameFilm,place,timeStartFilm,Kategory);
        if (Kategory == 0){
            if (pologenie.getNK0()<1)
                throw  new UnknownInputException();
            pologenie.setNK0(pologenie.getNK0()-1);
        }
        else{
            if (pologenie.getNK1()<1)
                throw  new UnknownInputException();
            pologenie.setNK0(pologenie.getNK1()-1);
        }
        Repository.write();
    }

    @Override
    public int ZabTicket(String day, String nameFilm, String place, String timeStartFilm,int Kategory) throws UnknownInputException, IOException {
        int RandomNumber;
        boolean flag = false;
        Pologenie pologenie = Poisk(day,nameFilm,place,timeStartFilm,Kategory);
        Random objGenerator = new Random();
        if (Kategory == 0) {
            if (pologenie.getNK0()<1)
             throw  new UnknownInputException();
            pologenie.setNK0(pologenie.getNK0() - 1);
            pologenie.setNBK0(pologenie.getNBK0() + 1);
            do {
                RandomNumber = objGenerator.nextInt(10000);
                for (Integer kod : pologenie.getListOfBron0())
                {
                    if(RandomNumber == kod){
                        flag = true;
                        break;
                    }
                }
            }while (flag);
            pologenie.getListOfBron0().add(RandomNumber);
        }
        else {
            if (pologenie.getNK1()<1)
                throw  new UnknownInputException();
            pologenie.setNK1(pologenie.getNK1() - 1);
            pologenie.setNBK1(pologenie.getNBK1() + 1);
            do {
                RandomNumber = objGenerator.nextInt(10000);
                for (Integer kod : pologenie.getListOfBron1()) {
                    if(RandomNumber == kod){
                        flag = true;
                        break;
                    }
                }
            }while (flag);
            pologenie.getListOfBron1().add(RandomNumber);
        }
        Repository.write();
        return RandomNumber;
    }

    @Override
    public void buyZabTicket(String day, String nameFilm, String place, String timeStartFilm,int Kategory, int idBron) throws UnknownInputException, IOException {
        boolean flag = false;
        Pologenie pologenie = Poisk(day,nameFilm,place,timeStartFilm,Kategory);
        if (Kategory == 0) {
            if (pologenie.getNBK0()<1)
                throw  new UnknownInputException();
            Iterator<Integer> ListOfBron = pologenie.getListOfBron0().iterator();
            while(ListOfBron.hasNext()) {
                int nextBron = ListOfBron.next();
                if (nextBron == (idBron)) {
                    ListOfBron.remove();
                    pologenie.setNBK0(pologenie.getNBK0() - 1);
                    flag = true;
                    break;
                }
            }
        }
        else {
            if (pologenie.getNBK1()<1)
                throw  new UnknownInputException();
            Iterator<Integer> ListOfBron = pologenie.getListOfBron1().iterator();
            while (ListOfBron.hasNext()) {
                int nextBron = ListOfBron.next();
                if (nextBron == (idBron)) {
                    ListOfBron.remove();
                    pologenie.setNBK0(pologenie.getNBK1() - 1);
                    flag = true;
                    break;
                }
            }
        }
        if(flag)
            Repository.write();
        else throw new UnknownInputException();
    }
}
