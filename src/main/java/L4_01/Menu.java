package L4_01;

import L4_01.exception.NotEnoughMoneyException;
import L4_01.exception.UnknownAccountException;
import L4_01.service.FileAccountService;

import java.io.IOException;

public class Menu {
    public static boolean quit;

    void starter(String what) throws UnknownAccountException, NotEnoughMoneyException, IOException {
        String[] action = what.split(" ");
        switch (action[0]) {
            case "balance":
                System.out.println(FileAccountService.balance(Integer.parseInt(action[1])));
                break;
            case "withdraw":
                FileAccountService.withdraw(Integer.parseInt(action[1]),Double.parseDouble(action[2]));
                System.out.println(FileAccountService.balance(Integer.parseInt(action[1])));
                break;
            case "deposite":
                FileAccountService.deposit(Integer.parseInt(action[1]),Double.parseDouble(action[2]));
                System.out.println(FileAccountService.balance(Integer.parseInt(action[1])));
                break;
            case "transfer":
                FileAccountService.transfer(Integer.parseInt(action[1]),Integer.parseInt(action[2]),Double.parseDouble(action[3]));
                System.out.println(FileAccountService.balance(Integer.parseInt(action[1])));
                System.out.println(FileAccountService.balance(Integer.parseInt(action[2])));
                break;
            case "quit":
                quit = true;
                break;
        }
        System.out.println();
    }

    void showmenu(){
        System.out.println("Меню:");
        System.out.println("    - при вводе в консоле команды balance [id] – вывеси информацию о счёте");
        System.out.println("    - при вводе в консоле команды withdraw [id] [amount] – снять указанную сумму");
        System.out.println("    - при вводе в консоле команды deposite [id] [amount] – внести на счет указанную сумму");
        System.out.println("    - при вводе в консоле команды transfer [from] [to] [amount] – перевести сумму с одного счета на другой");
        System.out.println("    - при вводе в консоле команды quit - Завершить выполнение программы");
    }
}
