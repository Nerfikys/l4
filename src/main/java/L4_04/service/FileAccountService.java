package L4_04.service;


import L4_04.domain.Account;
import L4_04.exception.NotEnoughMoneyException;
import L4_04.exception.UnknownAccountException;



import L4_04.repositry.Repository;
import org.springframework.beans.factory.annotation.Autowired;



import java.io.IOException;



public class FileAccountService implements AccountService {


    @Autowired
    private Repository Repository;

     public void withdraw(int accountId, double amount) throws NotEnoughMoneyException, UnknownAccountException, IOException {
        Account account = null;
        for (Account acc: store.ListOfAccount) {
                if (acc.getId() == accountId)
                {
                    account = acc;
                    break;
                }
        }
        if(account == null) {
            throw new UnknownAccountException();
        }
        if (account.getAmount() < amount) {
            throw new NotEnoughMoneyException();
        }
         account.setAmount(account.getAmount()-amount);
         Repository.write(account);
    }

    public double balance(int accountId) throws UnknownAccountException {
        Account account = null;
        for (Account acc: store.ListOfAccount) {
            if (acc.getId() == accountId)
            {
                account = acc;
                break;
            }
        }
        if(account == null) {
            throw new UnknownAccountException();
        }
        return account.getAmount();
    }

    public void deposit(int accountId, double amount) throws UnknownAccountException, IOException {
        Account account = null;
        for (Account acc: store.ListOfAccount) {
            if (acc.getId() == accountId)
            {
                account = acc;
                break;
            }
        }
        if(account == null) {
            throw new UnknownAccountException();
        }
        account.setAmount(account.getAmount()+amount);
        Repository.write(account);
    }


    public void transfer(int fromId, int toId, double amount) throws NotEnoughMoneyException, UnknownAccountException, IOException {

        Account account = null;
        Account account2 = null;
        for (Account acc: store.ListOfAccount) {
            if (acc.getId() == fromId)
            {
                account = acc;
                break;
            }
        }
        for (Account acc: store.ListOfAccount) {
            if (acc.getId() == toId)
            {
                account2 = acc;
                break;
            }
        }
        if((account == null)||(account2 == null)) {
            throw new UnknownAccountException();
        }
        if (account.getAmount() < amount) {
            throw new NotEnoughMoneyException();
        }
        account.setAmount(account.getAmount()-amount);
        account2.setAmount(account2.getAmount()+amount);
        Repository.write(account);
        Repository.write(account2);
    }
}
