package L4_02;



import L4_02.exception.NotEnoughMoneyException;
import L4_02.exception.UnknownAccountException;
import L4_02.factory.FactoryRepository;


import L4_02.repositry.Repository;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] arg) throws IOException, UnknownAccountException, NotEnoughMoneyException {
        Repository Repository = FactoryRepository.getRepository();
       Repository.read();
        Menu menu = new Menu();
        for (; ; ) {
            menu.showmenu();
            Scanner scanner = new Scanner(System.in);
            String choise = scanner.nextLine();
            System.out.println("\n");
            menu.starter(choise);
            if (Menu.quit){
                break;
            }
        }
    }
}
