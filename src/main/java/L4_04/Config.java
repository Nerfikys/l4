package L4_04;

import L4_04.repositry.FileRepository;
import L4_04.repositry.Repository;
import L4_04.service.AccountService;
import L4_04.service.FileAccountService;
import L4_04.service.store;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class Config {


    @Bean
    public Repository getRepository(){ return new FileRepository(); }

    @Bean
    public store getStore(){ return new store(); }

    @Bean
    public AccountService getAccountService(){return new FileAccountService(); }

}
