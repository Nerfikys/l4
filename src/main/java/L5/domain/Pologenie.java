package L5.domain;

import L5.service.Store;

import java.util.ArrayList;

public class Pologenie {//Одна строка расписания
    private String  day,//День
            film,//Фильм
            room;//Зал
    private int
            time,//Время
            NK0,//Число мест Категории 0
            NK1,//Число мест Категории 1
            NBK0,//Число броней Категории 0
            NBK1;//Число броней Категории 1
    private ArrayList<Integer>  ListOfBron0 = new ArrayList<>();//Список кодов брони для мест Категории 0
    private ArrayList<Integer> ListOfBron1 = new ArrayList<>();//Список кодов брони для мест Категории 1

    public Pologenie(String day, String film, String room, int time, int NK0, int NK1, int NBK0, int NBK1, ArrayList<Integer> listOfBron0, ArrayList<Integer> listOfBron1) {
        this.day = day;
        this.film = film;
        this.room = room;
        this.time = time;
        this.NK0 = NK0;
        this.NK1 = NK1;
        this.NBK0 = NBK0;
        this.NBK1 = NBK1;
        ListOfBron0 = listOfBron0;
        ListOfBron1 = listOfBron1;
    }

    public int getNBK0() {
        return NBK0;
    }

    public int getNBK1() {
        return NBK1;
    }

    public int getNK0() {
        return NK0;
    }

    public int getTime() {
        return time;
    }

    public ArrayList<Integer> getListOfBron1() {
        return ListOfBron1;
    }

    public String getDay() {
        return day;
    }

    public String getFilm() {
        return film;
    }

    public String getRoom() {
        return room;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setFilm(String film) {
        this.film = film;
    }

    public void setListOfBron0(ArrayList<Integer> listOfBron0) {
        ListOfBron0 = listOfBron0;
    }

    public void setListOfBron1(ArrayList<Integer> listOfBron1) {
        ListOfBron1 = listOfBron1;
    }

    public void setNBK0(int NBK0) {
        this.NBK0 = NBK0;
    }

    public void setNBK1(int NBK1) {
        this.NBK1 = NBK1;
    }

    public void setNK0(int NK0) {
        this.NK0 = NK0;
    }

    public void setNK1(int NK1) {
        this.NK1 = NK1;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getNK1() {
        return NK1;
    }

    public ArrayList<Integer> getListOfBron0() {
        return ListOfBron0;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
