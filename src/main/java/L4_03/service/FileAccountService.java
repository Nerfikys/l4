package L4_03.service;


import L4_03.domain.Account;
import L4_03.exception.NotEnoughMoneyException;
import L4_03.exception.UnknownAccountException;



import L4_03.repositry.Repository;
import org.springframework.beans.factory.annotation.Autowired;



import java.io.IOException;



public class FileAccountService implements AccountService {


    @Autowired
    private Repository Repository;

     public void withdraw(int accountId, double amount) throws NotEnoughMoneyException, UnknownAccountException, IOException {
         Account account = null;
        for (Account acc: store.usless) {
                if (acc.getId() == accountId)
                {
                    account = acc;
                    break;
                }
            }
               if(account == null) {
                   throw new UnknownAccountException();
               }
               if (account.getAmount() < amount) {
                   throw new NotEnoughMoneyException();
               }
         account.setAmount(account.getAmount()-amount);
         Repository.write(account);
    }

    public double balance(int accountId) throws UnknownAccountException {
        Account account = null;
        for (Account acc: store.usless) {
            if (acc.getId() == accountId)
            {
                account = acc;
                break;
            }
        }
        if(account == null) {
            throw new UnknownAccountException();
        }
        return account.getAmount();
    }

    public void deposit(int account, double amount) throws UnknownAccountException, IOException {
        boolean flag = false;
        int id = 0;
        for (int i =0;i<store.usless.size();i++) {
            if (store.usless.get(i).getId() == account)
            {
                flag = true;
                id = i;
            }
        }
        if(flag)
        {
            store.usless.get(id).setAmount(store.usless.get(id).getAmount()+amount);
            Repository.write(store.usless.get(id));
        }
    }


    public void transfer(int from, int to, double amount) throws NotEnoughMoneyException, UnknownAccountException, IOException {

        boolean flag = false;
        boolean flag2 = false;
        int id = 0;
        int id2 = 0;
        for (int i =0;i<store.usless.size();i++) {
            if (store.usless.get(i).getId() == from)
            {
                flag = true;
                id = i;
            }
        }
        for (int i =0;i<store.usless.size();i++) {
            if (store.usless.get(i).getId() == to)
            {
                flag2 = true;
                id2 = i;
            }
        }
        if(flag&&flag2)
        {
            if (store.usless.get(id).getAmount() >= amount) {
                store.usless.get(id).setAmount(store.usless.get(id).getAmount()-amount);
                store.usless.get(id2).setAmount(store.usless.get(id2).getAmount()+amount);
                Repository.write(store.usless.get(id));
                Repository.write(store.usless.get(id2));
            } else throw new NotEnoughMoneyException();
        }
        else throw new UnknownAccountException();
    }
}
