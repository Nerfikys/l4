package L5.domain;

public class Film {
    private  int id;
    private  String name;
    private int time;
    private  int price;


    public Film(){
    }
    public Film(int id,String name,int time,int price){
        this.id=id;
        this.name=name;
        this.time=time;
        this.price=price;
    }

    public int getId(){ return id; }
    public void setId(int id){ this.id = id; }

    public int getTime() { return time; }
    public void setTime(int time) { this.time = time; }

    public int getPrice() { return price; }
    public void setPrice(int price) { this.price = price; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
}
