package L5.service;

import L5.domain.Pologenie;
import L5.exception.UnknownInputException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
public interface TicketService {



   void buyTicket(String day, String nameFilm, String place, String timeStartFilm,int Kategory) throws UnknownInputException, IOException;
   int ZabTicket(String day,String nameFilm,String place,String timeStartFilm,int Kategory) throws UnknownInputException, IOException;
   void buyZabTicket(String day,String nameFilm,String place,String timeStartFilm,int Kategory,int idBron) throws UnknownInputException, IOException;
   default Pologenie Poisk(String day,String nameFilm,String place,String timeStartFilm,int Kategory)throws UnknownInputException{
      String[] splited = timeStartFilm.split(":");
      int time = ((Integer.parseInt(splited[0]))*60)+(Integer.parseInt(splited[1]));
      for(Pologenie pologenie: Store.Raspisanie)
      if(day.equals(pologenie.getDay()))
         if(nameFilm.equals(pologenie.getFilm()))
            if (place.equals(pologenie.getRoom()))
               if (time == (pologenie.getTime()))
                  return pologenie;
      throw new UnknownInputException();
   }
}
