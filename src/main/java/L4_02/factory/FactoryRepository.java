package L4_02.factory;

import L4_02.repositry.FileRepository;
import L4_02.repositry.Repository;

public class FactoryRepository {
    public static Repository getRepository(){
        return new FileRepository();
    }
}
