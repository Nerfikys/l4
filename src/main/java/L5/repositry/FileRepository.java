package L5.repositry;


import L5.domain.Film;
import L5.domain.Pologenie;
import L5.domain.Room;
import L5.service.Store;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

@Component
public class FileRepository implements Repository {
    static File adres;
    static  FileReader fr;
    static  BufferedReader reader;

    static   {
        try {
                read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void write() throws IOException {
        FileWriter writer;
        for (Film film: Store.ListOfFilm) {
            writer = new FileWriter("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\films\\" + film.getId() + ".txt");
            writer.write(film.getName() + "\n" + film.getTime()+"\n"+film.getPrice());
            writer.flush();
        }
        for (Room room: Store.ListOfRoom) {
            writer = new FileWriter("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\rooms\\" + room.getId() + ".txt");
            writer.write(room.getName() + "\n" + room.getPlaceKat0()+"\n" + room.getPlaceKat1() );
            writer.flush();
        }
        int i = 0;
        for (Pologenie List: Store.Raspisanie){
            writer = new FileWriter("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\Rasp\\" + i + ".txt");
            i++;
            ArrayList<Integer> ForWrite;
            writer.write(List.getDay()+"\n"+List.getFilm()+"\n"+List.getRoom()+"\n"+List.getTime()+"\n"+List.getNK0()+"\n"+List.getNK1()+"\n"+List.getNBK0()+"\n"+List.getNBK1()+"\n");
            for (Integer kod:List.getListOfBron0()){
                writer.write(kod+"\n");
            }
            for (Integer kod:List.getListOfBron1()){
                writer.write(kod+"\n");
            }
            writer.flush();
        }
    }
    public static void read() throws IOException {
        Path path = Paths.get("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\films");
        if (!Files.exists(path)) {
            new File("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\films").mkdirs();
            Film film = new Film(0, "Афера", 103, 200);
            Store.ListOfFilm.add(film);
            film = new Film(1, "Иллюзии", 85, 250);
            Store.ListOfFilm.add(film);
            film = new Film(2, "Взаперти", 90, 300);
            Store.ListOfFilm.add(film);
            film = new Film(3, "Горизонт", 93, 200);
            Store.ListOfFilm.add(film);
            film = new Film(4, "Интелект", 119, 250);
            Store.ListOfFilm.add(film);
            film = new Film(5, "Алиса", 96, 200);
            Store.ListOfFilm.add(film);
            film = new Film(6, "Союз", 89, 200);
            Store.ListOfFilm.add(film);
            film = new Film(7, "Остриё", 118, 300);
            Store.ListOfFilm.add(film);
            film = new Film(8, "Сказка", 83, 200);
            Store.ListOfFilm.add(film);
            film = new Film(9, "Амнезия", 107, 300);
            Store.ListOfFilm.add(film);
        } else {
            File file = new File("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\films\\");
            File[] listOfFiles = file.listFiles();
            String Name;
            int id;
            int time;
            int price;
            for (int i = 0; i < listOfFiles.length; i++) {
                Name = listOfFiles[i].getName().replace(".txt", "");
                id = Integer.parseInt(Name);
                File adres = new File("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\films\\" + Name + ".txt");
                //создаем объект FileReader для объекта File
                fr = new FileReader(adres);
                //создаем BufferedReader с существующего FileReader для построчного считывания
                reader = new BufferedReader(fr);
                // считаем сначала первую строку
                Name = reader.readLine();
                // считываем остальные строки
                time = Integer.parseInt(reader.readLine());
                price = Integer.parseInt(reader.readLine());
                Film film = new Film(id, Name, time, price);
                Store.ListOfFilm.add(film);
            }
        }
        path = Paths.get("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\rooms");
        if (!Files.exists(path)) {
            new File("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\rooms").mkdirs();
            Room zal = new Room(0, "Люмьер", 45,4);
            Store.ListOfRoom.add(zal);
            zal = new Room(1, "Феллини", 40,8);
            Store.ListOfRoom.add(zal);
        } else {
            File file = new File("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\rooms\\");
            File[] listOfFiles = file.listFiles();
            String name;
            int idR;
            int placeKat0;
            int placeKat1;
            for (int i = 0; i < listOfFiles.length; i++) {
                name = listOfFiles[i].getName().replace(".txt", "");
                idR = Integer.parseInt(name);
                adres = new File("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\rooms\\" + name + ".txt");
                //создаем объект FileReader для объекта File
                fr = new FileReader(adres);
                //создаем BufferedReader с существующего FileReader для построчного считывания
                reader = new BufferedReader(fr);
                // считаем сначала первую строку
                name = reader.readLine();
                // считываем остальные строки
                placeKat0 = Integer.parseInt(reader.readLine());
                placeKat1 = Integer.parseInt(reader.readLine());
                Room zal = new Room(idR, name, placeKat0,placeKat1);
                Store.ListOfRoom.add(zal);
            }

        }
        path = Paths.get("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\Rasp");
        if (!Files.exists(path)) {
            new File("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\Rasp").mkdirs();
            ArrayList<Integer> ListOfBron0 = new ArrayList<>();//Список кодов брони
            ArrayList<Integer> ListOfBron1 = new ArrayList<>();//Список кодов брони
            Pologenie pologenie = new Pologenie("понедельник","Афера","Люмьер",540,Store.ListOfRoom.get(0).getPlaceKat0(),Store.ListOfRoom.get(0).getPlaceKat1(),0,0,ListOfBron0,ListOfBron1);//Одна строка расписания
            Store.Raspisanie.add(pologenie);
            ListOfBron0 = new ArrayList<>();//Список кодов брони
            ListOfBron1 = new ArrayList<>();//Список кодов брони
            pologenie = new Pologenie("понедельник","Иллюзии","Феллини",570,Store.ListOfRoom.get(1).getPlaceKat0(),Store.ListOfRoom.get(1).getPlaceKat1(),0,0,ListOfBron0,ListOfBron1);//Одна строка расписания
            Store.Raspisanie.add(pologenie);
        } else {
            File file = new File("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\Rasp\\");
            File[] listOfFiles = file.listFiles();
            Pologenie pologenie;
            String id,Film,Room,day;
            int time,NK0,NK1,NBK0,NBK1;
            ArrayList<Integer> ListOfBron0;
            ArrayList<Integer> ListOfBron1;
            for (int i = 0; i < listOfFiles.length; i++) {

                id = listOfFiles[i].getName().replace(".txt", "");
                adres = new File("C:\\Users\\Megabit\\Downloads\\project\\A\\src\\main\\java\\L5\\Rasp\\" + id + ".txt");
                //создаем объект FileReader для объекта File
                fr = new FileReader(adres);
                //создаем BufferedReader с существующего FileReader для построчного считывания
                reader = new BufferedReader(fr);
                // считаем сначала первую строку
                day = reader.readLine();
                // считываем остальные строки
                Film = reader.readLine();
                Room = reader.readLine();
                time = Integer.parseInt(reader.readLine());
                NK0 = Integer.parseInt(reader.readLine());
                NK1 = Integer.parseInt(reader.readLine());
                NBK0 = Integer.parseInt(reader.readLine());
                NBK1 = Integer.parseInt(reader.readLine());
                ListOfBron0 = new ArrayList<>();
                for(int j=0;j<NBK0;j++){
                    ListOfBron0.add(Integer.parseInt(reader.readLine()));
                }
                ListOfBron1 = new ArrayList<>();
                for(int j=0;j<NBK1;j++){
                    ListOfBron1.add(Integer.parseInt(reader.readLine()));
                }
                pologenie = new Pologenie(day,Film,Room,time,NK0,NK1,NBK0,NBK1,ListOfBron0,ListOfBron1);
                Store.Raspisanie.add(pologenie);
            }
        }

    }
}
