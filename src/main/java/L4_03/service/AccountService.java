package L4_03.service;


import L4_03.exception.NotEnoughMoneyException;
import L4_03.exception.UnknownAccountException;
import org.springframework.stereotype.Component;

import java.io.IOException;
public interface AccountService {
   void withdraw (int account, double amount) throws UnknownAccountException, NotEnoughMoneyException, IOException;
   double balance(int account) throws UnknownAccountException;
   void deposit(int account, double amount) throws UnknownAccountException, NotEnoughMoneyException, IOException;
   void transfer(int from, int to, double amount) throws UnknownAccountException, NotEnoughMoneyException, IOException;
}
