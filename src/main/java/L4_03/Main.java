package L4_03;

import L4_03.exception.NotEnoughMoneyException;
import L4_03.exception.UnknownAccountException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import java.io.IOException;
import java.util.Scanner;

@SpringBootApplication
public class Main implements CommandLineRunner {

    @Autowired
    Menu menu;



    public static void main(String[] args) throws IOException, UnknownAccountException, NotEnoughMoneyException {
        SpringApplication.run(Main.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        for (; ; ) {
            menu.showmenu();
            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();
            System.out.println("\n");
            menu.starter(choice);
            if (Menu.quit) {
                break;
            }
        }
    }
}
