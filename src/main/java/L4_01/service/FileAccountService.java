package L4_01.service;

import L4_01.exception.NotEnoughMoneyException;
import L4_01.exception.UnknownAccountException;
import L4_01.repositry.FileRepository;
import java.io.IOException;


public class FileAccountService implements AccountService {

    static public void withdraw(int account, double amount) throws NotEnoughMoneyException, UnknownAccountException, IOException {
       // FileRepository.read(account);
       // FileRepository.write(account,FileRepository.name,(FileRepository.number-amount));

        if (FileRepository.read(account)){
            if(FileRepository.number >= amount)
            {
                FileRepository.write(account, FileRepository.name, FileRepository.number-amount);
            }
            else throw new NotEnoughMoneyException();
        }
        else throw new UnknownAccountException();

    }


    static public double balance(int account) throws UnknownAccountException {
        //FileRepository.read(account);
        //return (FileRepository.number);

        if (FileRepository.read(account)){
            FileRepository.read(account);
            return (FileRepository.number);
        }
        else throw new UnknownAccountException();

    }


    static public void deposit(int account, double amount) throws UnknownAccountException, IOException {
        //FileRepository.read(account);
        //FileRepository.write(account,FileRepository.name,(FileRepository.number+amount));

        if (FileRepository.read(account)){
            FileRepository.read(account);
            FileRepository.write(account,FileRepository.name,(FileRepository.number+amount));
        }
        else throw new UnknownAccountException();
    }


    static public void transfer(int from, int to, double amount) throws NotEnoughMoneyException, UnknownAccountException, IOException {
        //FileRepository.read(from);
        //FileRepository.write(from,FileRepository.name,(FileRepository.number-amount));
        //FileRepository.read(to);
        //FileRepository.write(to,FileRepository.name,(FileRepository.number+amount));

        if ((FileRepository.read(from)) && (FileRepository.read(to))){
            FileRepository.read(from);
            if(FileRepository.number >= amount){
                FileRepository.write(from,FileRepository.name,(FileRepository.number-amount));
                FileRepository.read(to);
                FileRepository.write(to,FileRepository.name,(FileRepository.number+amount));
            }
            else throw new NotEnoughMoneyException();
        }
        else throw new UnknownAccountException();


    }

}
