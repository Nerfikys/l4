package L5.domain;


import java.util.ArrayList;

public class Room {
    private  int id;
    private  String name;
    private  int placeKat0;
    private  int placeKat1;



    public Room(){
    }

    public Room(int id, String name, int placeKat0,int placeKat1) {
        this.id = id;
        this.name = name;
        this.placeKat0 = placeKat0;
        this.placeKat1 = placeKat1;
    }


    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id = id;
    }

    public void setName(String name) { this.name = name; }
    public String getName() { return name; }

    public int getPlaceKat0() { return placeKat0; }
    public void setPlaceKat0(int placeKat0) { this.placeKat0 = placeKat0; }

    public int getPlaceKat1() { return placeKat1; }
    public void setPlaceKat1(int placeKat1) { this.placeKat1 = placeKat1; }
}
