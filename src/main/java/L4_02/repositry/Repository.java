package L4_02.repositry;

import L4_02.domain.Account;

import java.io.IOException;

public interface Repository {
   void write(Account out) throws IOException;
   void  read() throws IOException;

}
