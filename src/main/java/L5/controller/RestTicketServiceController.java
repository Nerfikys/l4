package L5.controller;


import L5.exception.UnknownInputException;
import L5.repositry.FileRepository;
import L5.repositry.Repository;
import L5.service.Store;
import L5.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.io.IOException;

@Controller
public class RestTicketServiceController {

@Autowired
    TicketService ticketService;

    @GetMapping("/buyTicket")
    public ResponseEntity<Integer> buyTicket(@RequestParam String day,@RequestParam String nameFilm,@RequestParam String place,@RequestParam String timeStartFilm,@RequestParam int Kategory) throws IOException, UnknownInputException {
        ticketService.buyTicket(day, nameFilm, place, timeStartFilm, Kategory);
        return new ResponseEntity<Integer>(HttpStatus.OK);
    }
    @GetMapping("/ZabTicket")
    public ResponseEntity<Integer> ZabTicket(@RequestParam String day,@RequestParam String nameFilm,@RequestParam String place,@RequestParam String timeStartFilm,@RequestParam int Kategory) throws IOException, UnknownInputException {
        int bron = ticketService.ZabTicket(day,nameFilm,place,timeStartFilm,Kategory);
        return new ResponseEntity<Integer>(bron,HttpStatus.OK);
    }@GetMapping("/buyZabTicket")
    public ResponseEntity<Integer> buyZabTicket(@RequestParam String day,@RequestParam String nameFilm,@RequestParam String place,@RequestParam String timeStartFilm,@RequestParam int Kategory,@RequestParam int idBron) throws IOException, UnknownInputException {
        ticketService.buyZabTicket(day, nameFilm, place, timeStartFilm, Kategory,idBron);
        return new ResponseEntity<Integer>(HttpStatus.OK);
    }


}
