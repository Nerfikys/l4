package L4_04.service;


import L4_04.exception.NotEnoughMoneyException;
import L4_04.exception.UnknownAccountException;


import java.io.IOException;
public interface AccountService {
   void withdraw (int accountId, double amount) throws UnknownAccountException, NotEnoughMoneyException, IOException;
   double balance(int accountId) throws UnknownAccountException;
   void deposit(int accountId, double amount) throws UnknownAccountException, IOException;
   void transfer(int fromId, int toId, double amount) throws UnknownAccountException, NotEnoughMoneyException, IOException;
}
