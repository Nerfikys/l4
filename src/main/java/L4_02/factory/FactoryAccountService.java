package L4_02.factory;

import L4_02.service.AccountService;
import L4_02.service.FileAccountService;

public class FactoryAccountService {
    public static AccountService getAccountService(){
            return new FileAccountService();
    }
}
