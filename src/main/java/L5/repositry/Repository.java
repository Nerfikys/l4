package L5.repositry;

import org.springframework.stereotype.Component;

import java.io.IOException;
@Component
public interface Repository {
   void write() throws IOException;
   static void read() throws IOException{};

}
