package L4_01;


import L4_01.exception.NotEnoughMoneyException;
import L4_01.exception.UnknownAccountException;
import L4_01.repositry.FileRepository;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, UnknownAccountException, NotEnoughMoneyException {
        Path path = Paths.get("C:\\Users\\komerbek\\YandexDisk\\МТУСИ\\7 Семестр\\РВ\\L4\\Acc");
        if (!Files.exists(path)) {
            new File("C:\\Users\\komerbek\\YandexDisk\\МТУСИ\\7 Семестр\\РВ\\L4\\Acc").mkdirs();
            FileRepository.write(0, "Koma", 40000);
            FileRepository.write(1111, "Oleg", 35000);
            FileRepository.write(2222, "Korg", 38000);
            FileRepository.write(3333, "Socr", 85000);
            FileRepository.write(4444, "Vlad", 12000);
            FileRepository.write(5555, "Anna", 73000);
            FileRepository.write(6666, "Lena", 42000);
            FileRepository.write(7777, "Max", 21000);
            FileRepository.write(8888, "Tal", 94000);
            FileRepository.write(9999, "Jesus", 27000);
        }

        Menu menu = new Menu();
        for (; ; ) {
            menu.showmenu();
            Scanner scanner = new Scanner(System.in);
            String choise = scanner.nextLine();
            System.out.println("\n");
            menu.starter(choise);
            if (Menu.quit){
                break;
            }
        }
    }
}
